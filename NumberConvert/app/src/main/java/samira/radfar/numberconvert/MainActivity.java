package samira.radfar.numberconvert;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText et;
    Button btn;
    TextView tv;
    int[] StringNO = new int[10];
    String[] yekan = {"zero","one","two","three","four","five","six","seven","eight","nine"};
    String[] dahgan = {"","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"};
    String[] sadgan = {"","hundred","two hundred","three hundred","four hundred","five hundred","six hundred","seven hundred","eight hundred","nine hundred"};
    String[] dahganEstesna = {"ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};
    String text="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.btn);
        et = (EditText) findViewById(R.id.et);
        tv= (TextView) findViewById(R.id.tv);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text="";
                int no = Integer.parseInt(et.getText().toString());
                int lenght = String.valueOf(no).length();
//                Toast.makeText(MainActivity.this,String.valueOf(no),Toast.LENGTH_SHORT);

                SetStringArray(no,lenght,StringNO);
                ConvertToLetters(lenght,StringNO);

            }
        });
    }

    private void SetStringArray(int no,int length,int[] StringNO)
    {
        int i = 0;
        int l1 = length;
        while (l1 > 1)
        {
            StringNO[i] = (int) (no % 10);
            no =(int) Math.floor(no/10);
            i++;
            l1--;
        }
        StringNO[i] = no;
        tv.setText(String.valueOf(length));
    }

    private void ConvertToLetters(int length,int[] StringNO)
    {
        int i=length;
        Boolean Exeption=false;
        while (i>=0 && Exeption==false) {
            switch (--i) {
                case 0:
                {
                    if (length > 1 && StringNO[i]==0)
                        break;
                    else
                    {
                        text=text+" "+ yekan[StringNO[i]];
                    }
                    break;
                }
                case 1:
                {
                    if (StringNO[i]==1) {
                        text = text + " " + dahganEstesna[StringNO[i - 1]];
                        Exeption=true;
                    }
                    else
                        text=text+" "+dahgan[StringNO[i]];
                    break;
                }
                case 2:
                {
                    text=text+" "+sadgan[StringNO[i]];
                    break;
                }
            }
        }
        tv.setText(text);
    }
}
